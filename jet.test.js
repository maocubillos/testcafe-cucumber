import errorHandling from "./features/support/errorHandling.js";
import { waitForReact } from "testcafe-react-selectors";
import testControllerHolder from "./features/support/testControllerHolder.js";

fixture("fixture")
    .page("https://notjet.net")
    .beforeEach(async () => {
        await waitForReact(10000);
    });

test("test", testControllerHolder.capture)
    .after(async t => {await errorHandling.ifErrorTakeScreenshot(t)});
