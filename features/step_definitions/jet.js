const {Given, When, Then} = require('cucumber');
const Selector = require('testcafe').Selector;

Given('I am open Jet\'s page', async function() {
    await testController.navigateTo('https://notjet.net');
});

Given('Set my Zipcode as {string}', async function (string) {
    const dropDownButton = Selector('.kpNrxn').with({boundTestRun: testController});
    const zipcodeLink = Selector('#react-aria-modal-dialog').find('.sc-htpNat:not([href])').with({boundTestRun: testController});

    await testController.click(dropDownButton);
    await testController.click(zipcodeLink);
});

Given('I navigate to the main Category {string}', async function(category) {
    const hamburguerIcon = Selector('.kySRNi').with({boundTestRun: testController});
    const categoryLink = Selector('.AnimatedMenuItem-qlvyd0-0').withText(category);

    await testController.click(hamburguerIcon);
    await testController.click(categoryLink);
})

Given('I login in the app', async function() {
    const dropDownButton = Selector('.kpNrxn').with({boundTestRun: testController});
    const hamburguerIcon = Selector('.kySRNi').with({boundTestRun: testController});
    const isDesktopMenu = await dropDownButton.visible;
    const loginDesktopLink = Selector('#react-aria-modal-dialog').find('.jHJfpH[href="/register"]').with({boundTestRun: testController});
    const loginMobileLink = Selector('.AnimatedMenuItem-qlvyd0-0').withText('Sign in / Register');
    const loginEmailInput = Selector('#login-email').with({boundTestRun: testController});
    const loginPasswordInput = Selector('#login-password').with({boundTestRun: testController});
    const submitButton = Selector('.field.submit').find('button').with({boundTestRun: testController});
    const menuButton = isDesktopMenu ? dropDownButton : hamburguerIcon;
    const loginLink = isDesktopMenu ? loginDesktopLink : loginMobileLink;
    const email = 'mauricio.cubillos@jet.com';
    const password = 'Zemoga2019';

    await testController.click(menuButton);
    await testController.click(loginLink);
    await testController.typeText(loginEmailInput, email);
    await testController.typeText(loginPasswordInput, password);
    await testController.click(submitButton);
});

When('I am browse the dropdown menu, select the vertical {string} then the category {string} and select {string}', async function(vertical, category, page) {
    const dropDownButton = Selector('.iljEcZ').find('span[role="button"]').with({boundTestRun: testController});
    const verticalLink = Selector('.lisSDX').withText(vertical);
    const categoryLink = Selector('.lisSDX').withText(category);
    const pageLink = Selector('.lisSDX').withText(page);

    await testController.click(dropDownButton);
    await testController.click(verticalLink);
    await testController.click(categoryLink);
    await testController.click(pageLink);
})

When('I am typing my search request {string} on Jet', async function(text) {
    var input = Selector('#MobileSearchBarInput').with({boundTestRun: testController});
    await this.addScreenshotToReport();
    await testController.click(input);
    await testController.typeText(input, text);
});

When('I click in a main category link, like {string}', async function(category) {
    const dropDownButton = Selector('.kpNrxn').with({boundTestRun: testController});
    const hamburguerIcon = Selector('.kySRNi').with({boundTestRun: testController});
    const isDesktopMenu = await dropDownButton.visible;
    const menuButton = isDesktopMenu ? dropDownButton : hamburguerIcon;
    const categoryLink = Selector('.AnimatedMenuItem-qlvyd0-0').withText(category);

    await testController.click(menuButton);
    await testController.click(categoryLink);
})

Then('I should see the page for the main category {string}', async function(category) {
    const pageTitle = Selector('.iljEcZ').find('span[role="button"]').with({boundTestRun: testController}).innerText;
    await testController.expect(pageTitle).contains(category);
})

Then('I press the {string} key on Jet', async function(text) {
    await testController.pressKey(text);
});

Then('I should see that the result includes products with {string}', async function(text) {
    var firstLink = Selector(`.jjhDbk`).with({boundTestRun: testController});
    var fistLinkLabel = firstLink.innerText;
    await testController.expect(fistLinkLabel).contains(text);
});

Then('I should see the breadcrumb with the correct route to {string}', async function (page) {
    const breadcrumbText = Selector('.krmiRB').with({boundTestRun: testController}).innerText;

    await testController.expect(breadcrumbText).contains(page);
})
