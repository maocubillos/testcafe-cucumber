Feature: Searching for Bananas by Jet

  I want to find Bananas in Jet.com as a guess user
  and as a logged user for a more specific results
  related with my area

  Scenario: Searching for bananas by Jet
    Given I am open Jet's page
    When I am typing my search request "Banana" on Jet
    Then I press the "enter" key on Jet
    Then I should see that the result includes products with "Banana"

  Scenario: Browse to the PLP page from home
    Given I am open Jet's page
    And I navigate to the main Category "Pantry & Household"
    When I am browse the dropdown menu, select the vertical "Baby" then the category "Diapering" and select "Diapers"
    Then I should see the breadcrumb with the correct route to "Baby"
  
  # Scenario Outline: Browse to main category page from homepage
  #   Given I am open Jet's page
  #   When I click in a main category link, like <category>
  #   Then I should see the page for the main category <title>

  #   Examples:
  #   | category | title |
  #   | "Pantry & Household" | "Pantry & Household" |
  #   | "Electronics" | "Electronics" |
  #   | "Home" | "Home" |

  # Scenario: Searching for bananas as a logged user in Jet
  #   Given I am open Jet's page
  #   And I login in the app
  #   When I am typing my search request "Banana" on Jet
  #   Then I press the "enter" key on Jet
  #   Then I should see that the result includes products with "Banana"

